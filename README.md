*READ ME

This project is a file collection for the course:

System Design in Computational Thinking at Tsinghua University.

The goal of this class is to familiar students with the tools and terminologies 
of modern computing systems.

The specific feature of this course is that it covers a full stack of computing 
systems, from basic logic chips to application systems.

**Content

Week 1   Logic, Design by Contract and Test Driven Development

Week 2   Arithmetic System, ALU, and Multi-Function Chips

Week 3   Memory, Delayed Flip-Flop, other memory modules and technologies


