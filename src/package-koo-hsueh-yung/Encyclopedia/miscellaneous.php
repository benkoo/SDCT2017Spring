<?php

function draw_board ($commands) {

   $image = imagecreate (235, 235);
   $white = imagecolorallocate ($image, 255, 255, 255);
   $black = imagecolorallocate ($image, 0, 0, 0);

   for ($x = 13; $x <= 234; $x += 26)
      imageline ($image, $x, 0, $x, 234, $black);

   for ($y = 13; $y <= 234; $y += 26)
      imageline ($image, 0, $y, 234, $y, $black);

   foreach ($commands as $current) {

      $horizontal = ord ($current[0]) - 97;
      $vertical = $current[1] - 1;
      $disposition = $current[2];

      $x = 13 + $horizontal * 26;
      $y = 13 + $vertical * 26;

      if ($current[2] == 'w') {

         imageellipse ($image, $x, $y, 20, 20, $black);
      }

      else if ($current[2] == 'b') {

         imagefilledellipse ($image, $x, $y, 20, 20, $black);
      }
   }

   return $image;
}

function parse_60eb ($subject, $start, $length) {

   $cursor = $start;
   $situation = 0;

   $buffer = '';
   $array = array ();

   while ($cursor < $length) {

      if ($situation == 0 && ord ($subject[$cursor]) != 91) {

         return array (1);
      }

      if ($situation == 0 && ord ($subject[$cursor]) == 91) {

         $situation = 1;
         $cursor++;
         continue;
      }

      if ($situation == 1 && ord ($subject[$cursor]) != 124 && ord ($subject[$cursor]) != 93) {

         $buffer = $buffer . $subject[$cursor];
         $cursor++;
         continue;
      }

      if ($situation == 1 && ord ($subject[$cursor]) == 124) {

         $array[] = trim ($buffer);
         $buffer = '';

         $cursor++;
         continue;
      }

      if ($situation == 1 && ord ($subject[$cursor]) == 93) {

         $array[] = trim ($buffer);

         $cursor++;

         return array (0, $array, $cursor);
      }

      return array (2);
   }

   return array (3);
}

function parse_fefa ($before) {

   $before = htmlspecialchars ($before);

   $after = '';
   $index = 0;
   $length = strlen ($before);

   $situation = 0;
   $figure = array ();
   $temporary = '';

   while ($index < $length) {

      if ($situation == 0 && ord ($before[$index]) == 10) {

         $after = $after . '<br>';
         $index++;
         continue;
      }

      if ($situation == 0 && ord ($before[$index]) == 91) {

         $result = parse_60eb ($before, $index, strlen ($before));

         if ($result[0] != 0)
            return 'PARSE ERROR';

         if (intval ($result[1][0]) == 272965037) {

            if (count ($result[1]) == 1)
               $after = $after . sprintf ('<img class="cd6c" src="/generate-board/%s">', urlencode (serialize (array ())));

            else
               $after = $after . sprintf ('<img class="cd6c" src="/generate-board/%s">', urlencode (serialize (array_slice ($result[1], 1))));

            $index = $result[2];
            continue;
         }

         if (count ($result[1]) == 1 && filter_var ($result[1][0], FILTER_VALIDATE_URL)) {

            $after = $after . sprintf ('<a class="e8a4" href="%s">%s</a>', $result[1][0], $result[1][0]);

            $index = $result[2];
            continue;
         }

         if (count ($result[1]) == 1) {

            $after = $after . sprintf ('<img class="cd6c" src="/retrieve/%s">', $result[1][0]);
            $index = $result[2];
            continue;
         }

         if (count ($result[1]) == 2 && filter_var ($result[1][0], FILTER_VALIDATE_URL)) {

            $after = $after . sprintf ('<a class="e8a4" href="%s">%s</a>', $result[1][0], $result[1][1]);

            $index = $result[2];
            continue;
         }

         if (count ($result[1]) == 2) {

            $after = $after . sprintf ('<a class="e8a4" href="%s">%s</a>', sprintf ('/read/%s', $result[1][0]), $result[1][1]);

            $index = $result[2];
            continue;
         }

         return 'PARSE ERROR';
      }

      if ($situation == 0) {

         $after = $after . $before[$index];
         $index++;
         continue;
      }

      if ($situation == 1 && ord ($before[$index]) != 93) {

         $temporary = $temporary . $before[$index];
         $index++;
         continue;
      }

      if ($situation == 1) {

         $pieces = explode ('|', $temporary);

         if (count ($pieces) == 1) {

            $after = $after . sprintf ('<img class="cd6c" src="/retrieve/%s">', $pieces[0]);
            $temporary = '';
            $situation = 0;
            $index++;
            continue;
         }

         $pieces[0] = trim ($pieces[0]);
         $pieces[1] = trim ($pieces[1]);

         if (filter_var ($pieces[0], FILTER_VALIDATE_URL)) {

            $after = $after . sprintf ('<a class="e8a4" href="%s">%s</a>', $pieces[0], $pieces[1]);
         }

         else {

            $after = $after . sprintf ('<a class="e8a4" href="%s">%s</a>', sprintf ('/read/%s', $pieces[0]), $pieces[1]);
         }

         $temporary = '';
         $situation = 0;
         $index++;
         continue;
      }
   }

   return $after;
}

function discern_f0d7 ($subject) {

   if (count ($subject) != 2)
      return false;

   if ($subject[0] != '')
      return false;

   if ($subject[1] != '')
      return false;

   return true;
}

function discern_f620 ($subject) {

   if (count ($subject) < 2)
      return false;

   if ($subject[0] != '')
      return false;

   if (in_array ('', array_slice ($subject, 1), true))
      return false;

   return true;
}

function discern_91c4 ($subject) {

   if (count ($subject) < 3)
      return false;

   if ($subject[0] != '')
      return false;

   if (in_array ('', array_slice ($subject, 1, count ($subject) - 2), true))
      return false;

   if ($subject[count ($subject) - 1] != '')
      return false;

   return true;
}

function get ($key, $default = null) {

   global $instance;

   return isset ($instance[1][$key])? $instance[1][$key]: $default;
}

function get_difference_7f96 ($past) {

   $difference = time () - $past;

   if ($difference < 60)
      { return sprintf ('%d %s', $difference, $difference == 1? 'second': 'seconds'); }

   if ($difference < 3600)
      { return sprintf ('%d %s', floor ($difference / 60), floor ($difference / 60) == 1? 'minute': 'minutes'); }

   if ($difference < 86400)
      { return sprintf ('%d %s', floor ($difference / 3600), floor ($difference / 3600) == 1? 'hour': 'hours'); }

   return sprintf ('%d %s', floor ($difference / 86400), floor ($difference / 86400) == 1? 'day': 'days');
}

function get_instance ($address, $request, $server, $server_name) {

   return array (split_114b (trim_ef73 ($address)), $request, $server, $server_name);
}

function get_random_547a () {

   $current = 0;
   $letters = '0123456789abcdef';
   $string  = chr (97 + rand (0, 5));

   while ($current++ < 7)
      { $string = sprintf ('%s%s', $string, $letters[rand (0, 15)]); }

   return $string;
}

function inject ($accusative, $dative, $bound_left, $bound_right) {

   $left  = substr ($dative, 0, strpos ($dative, $bound_left) + strlen ($bound_left));
   $right = substr ($dative, strpos ($dative, $bound_right, strpos ($dative, $bound_left) + strlen ($bound_left)));

   return sprintf ('%s%s%s', $left, $accusative, $right);
}

function lift ($rows) {

   return (count ($rows) == 0)? null: $rows[0];
}

function match () {

   global $instance;

   return match_cac1 ($instance[0], func_get_args ());
}

function match_0000 () {

   global $instance;

   $args = func_get_args ();

   if ($instance[3] != $args[0]) {

      return false;
   }

   return match_cac1 ($instance[0], array_slice (func_get_args (), 1));
}

function match_cac1 ($former, $latter) {

   if (count ($former) != count ($latter))
      { return false; }

   $count = count ($former);
   $loop  = 0;

   while ($loop < $count) {

      if ($latter[$loop] == '.')
         { $loop++; continue; }

      if ($former [$loop] != $latter [$loop])
         { return false; }

      $loop = $loop + 1;
   }

   return true;
}

function select_8c29 () {

   $arguments = func_get_args ();

   foreach ($arguments as $value) {

      if (! is_null ($value)) {

         return $value;
      }
   }

   return null;
}

function send_3e8f ($to, $subject, $text) {

   $curl = curl_init ();

   curl_setopt ($curl, CURLOPT_POST, 1);
   curl_setopt ($curl, CURLOPT_POSTFIELDS, array ('to'      => $to, 
                                                  'from'    => 'support@yuanengr.com', 
                                                  'bcc'     => 'temporary.fe4a@yuanengr.com', 
                                                  'subject' => $subject, 
                                                  'text'    => $text));
   curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
   curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
   curl_setopt ($curl, CURLOPT_URL, 'https://api.mailgun.net/v2/yuanengr.com/messages');
   curl_setopt ($curl, CURLOPT_USERPWD, 'api:key-ec621886b9f14987ff4c563353ee1888');

   return curl_exec ($curl);
}

function split_114b ($subject) {

   $subject = explode ('/', $subject);

   if (discern_f0d7 ($subject))
      return array ();

   if (discern_f620 ($subject))
      return array_slice ($subject, 1);

   if (discern_91c4 ($subject))
      return array_slice ($subject, 1, count ($subject) - 2);
}

function trim_ef73 ($string) {

   $position = strpos ($string, '?');

   if (is_bool ($position))
      { return $string; }

   return substr ($string, 0, $position);
}
