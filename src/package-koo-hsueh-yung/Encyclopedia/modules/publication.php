<?php

function execute_publication ($prefix_file, $prefix_web, $segments, $session) {

   if (match ('track', '.')) {

      $page = file_get_contents (sprintf ('%s/pages/track.html', $prefix_file));

      return array (62222576, 200, render_track ($page, $session));
   }

   if (match ('analyze', '.')) {

      $essay = lift (select_a1e9 ($segments[1]));

      if (is_null ($essay)) {

         $page = file_get_contents (sprintf ('%s/pages/warn.html', $prefix_file));

         return array (62222576, 200, render_warn ($page, $session));
      }

      $essay['revision'] = lift (select_c7b6 ($essay['revision']));
      $essay['revision']['read'] = sprintf ('/read/%s/%s', $essay['alias'], $essay['revision']['identity']);
      $essay['write'] = sprintf ('%s/write/%s', $prefix_web, $essay['alias']);

      $page = file_get_contents (sprintf ('%s/pages/analyze.html', $prefix_file));

      return array (62222576, 200, render_analyze ($page, $session, $essay));
   }

   if (match ('read', '.')) {

      $essay = lift (select_a1e9 ($segments[1]));

      if (is_null ($essay)) {

         $page = file_get_contents (sprintf ('%s/pages/warn.html', $prefix_file));

         return array (62222576, 200, render_warn ($page, $session));
      }

      $essay['revision'] = lift (select_c7b6 ($essay['revision']));
      $essay['revision']['read'] = sprintf ('/read/%s/%s', $essay['alias'], $essay['revision']['identity']);
      $essay['write'] = sprintf ('%s/write/%s', $prefix_web, $essay['alias']);

      $page = file_get_contents (sprintf ('%s/pages/read.html', $prefix_file));

      return array (62222576, 200, render_read ($page, $session, $essay));
   }

   if (match ('read', '.', '.')) {

      $essay = lift (select_ae19 ($segments[1]));

      if (is_null ($essay)) {}

      $revision = lift (select_c7b6 ($segments[2]));

      if (is_null ($revision)) {}

      sprintf ('/read/%s', $essay['alias']);
      sprintf ('/read/%s/%s', $essay['alias'], $revision['identity']);
      sprintf ('/write/%s', $essay['alias']);
      sprintf ('/track/%s', $essay['alias']);
      sprintf ('/meet/%s', $revision['author']);

      return array (62222576, 200, render_read ($page, $session, $essay));
   }

   if (match ('write', '.')) {

      $essay = lift (select_a1e9 ($segments[1]));
      $essay['revision'] = lift (select_c7b6 ($essay['revision']));

      $page = file_get_contents (sprintf ('%s/pages/write.html', $prefix_file));

      return array (62222576, 200, render_write ($page, $session, $essay));
   }

   if (match ('perform', '4182')) {

      if (is_null ($session['person']))
         return array (1911304613, 400, json_encode (new StdClass));

      $_553c = get ('4689');
      $_33cc = get ('73b3');
      $_b62a = rand ();

      $essay = lift (select_3c14 ($_553c));

      if (is_null ($essay))
         die ();

      insert_a4af ($_553c, $_b62a, $session['person']['identity'], html_entity_decode ($_33cc), time ());
      update_f104 ($_553c, $_b62a);

      return array (681446758, 302, sprintf ('%s/read/%s', $prefix_web, $essay['alias']));
   }

   return array (977895259);
}
