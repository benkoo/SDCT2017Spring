<?php

function render_manage ($page, $session) {

   return $page;
}

function execute_accomodation ($prefix_file, $prefix_web, $segments, $session) {

   if (match ('manage')) {

      $page = file_get_contents (sprintf ('%s/pages/manage.html', $prefix_file));

      return array (62222576, 200, render_manage ($page, $session));      
   }

   if (match ('generate-board', '.')) {

      $commands = unserialize (urldecode ($segments[1]));

      header ('Content-type: image/png');
      $image = draw_board ($commands);
      imagepng ($image);

      die ();
   }

   if (match ('perform', 'f70e')) {

      $_f581 = get ('0d77');
      $query = lift (select_877f ($_f581));

      if (is_null ($query)) {

         $identity = rand ();
         insert_c37f ($identity, $_f581);

         return array (681446758, 302, sprintf ('%s/search/%s', $prefix_web, $identity));         
      }

      return array (681446758, 302, sprintf ('%s/search/%s', $prefix_web, $query['identity']));
   }

   if (match ('explain')) {

      $page = file_get_contents (sprintf ('%s/pages/explain.html', $prefix_file));

      return array (62222576, 200, render_explain ($page, $session));
   }

   if (match ('retrieve', '.')) {

      $resource = lift (select_a628 ($segments[1]));

      echo file_get_contents (sprintf ('%s/assets/%s', $prefix_file, $resource['digest']));
      die ();
   }

   if (match ('retrieve', '.', '.')) {

      if (substr ($segments[2], strlen ($segments[2]) - 3, 3) == 'jpg')
         header ('Content-type: image/jpeg');

      if (substr ($segments[2], strlen ($segments[2]) - 3, 3) == 'pdf')
         header ('Content-type: application/pdf');

      if (substr ($segments[2], strlen ($segments[2]) - 3, 3) == 'png')
         header ('Content-type: image/png');

      if (substr ($segments[2], strlen ($segments[2]) - 3, 3) == 'tar')
         header ('Content-type: application/octet-stream');

      echo file_get_contents (sprintf ('%s/assets/%s', $prefix_file, $segments[1]));
      die ();
   }

   if (match () || match ('greet')) {

      $page = file_get_contents (sprintf ('%s/pages/greet.html', $prefix_file));
      $resources = select_cc55 ();

      return array (62222576, 200, render_greet ($page, $session, $resources));
   }

   if (match ('log-in')) {

      $page = file_get_contents (sprintf ('%s/pages/log-in.html', $prefix_file));

      return array (62222576, 200, $page);
   }

   if (match ('perform', 'fccb')) {

      $_bfff = get ('c588');
      $_ac34 = get ('d4cc');

      $authentication = lift (select_9599 ($_ac34));

      if (is_null ($authentication)) {

         die ('auth');
      }

      if (md5 ($_bfff) != $authentication['key']) {

         die ('key');
      }

      $session = rand ();
      insert_8af1 ($_SERVER['REMOTE_ADDR'], $session, 'zh', $authentication['person'], time ());
      setcookie ('b95a', $session, 0, '/');
      $session = lift (select_ec5f ($session));

      return array (681446758, 302, '/', 1212967921, $session);
   }

   if (match ('search', '.')) {

      $query = lift (select_b828 ($segments[1]));

      if (is_null ($query))
         return array (681446758, 302, sprintf ('%s/warn', $prefix_web), 1212967921, $session);

      $essay = lift (select_a1e9 ($query['substance']));

      if (! is_null ($essay))
         return array (681446758, 302, sprintf ('%s/read/%s', $prefix_web, $essay['alias']), 1212967921, $session);

      $page = file_get_contents (sprintf ('%s/pages/warn.html', $prefix_file));

      return array (62222576, 400, render_warn ($page, $session));
   }

   if (match ('view-profile', '.')) {

      
   }

   if (match ('list-essays')) {

      $essays = select_b2df ();
      $page = file_get_contents (sprintf ('%s/pages/list-essays.html', $prefix_file));

      return array (62222576, 200, render_list_essays ($page, $session, $essays));
   }

   if (match ('log-out')) {

      delete_5655 ($session['identity']);
      $identity = rand ();
      insert_8af1 ($_SERVER['REMOTE_ADDR'], $identity, 'zh', null, time ());
      setcookie ('b95b', $identity, 0, '/');
      $session = lift (select_ec5f ($identity));

      return array (681446758, 302, '/', 815015088, $session);
   }

   return array (977895259);
}
