<?php

function get_d611 ($value) {

   $_f17d = 0;
   $_cdbb = '';

   while ($_f17d < strlen ($value)) {

      $_cdbb .= ord ($value[$_f17d]) . ',';

      $_f17d++;
   }

   return $_cdbb;
}

function render_analyze ($page, $session, $essay) {

   $text = $essay['revision']['text'];
   $segments = array ();

   while (strlen ($text) > 0) {

      $segments[] = substr ($text, 0, 12);
      $text = substr ($text, 12);
   }

   $_ee8f = '';

   foreach ($segments as $key => $value) {

      $_825a = sprintf ('<td class="fe8d">%s</td>', $value);
      $_2c30 = sprintf ('<td class="fe8d">%s</td>', get_d611 ($value));
      $_8147 = sprintf ('<tr class="a403">%s%s</tr>', $_825a, $_2c30);
      $_ee8f = sprintf ('%s%s', $_ee8f, $_8147);
   }

   $page = inject (sha1 ($essay['revision']['text']), $page, '<th class="fe8d" id="f643" colspan="2">', '</th>');
   $page = inject (sprintf ('&#26368;&#26032;&#29256;&#26412;&#65306;%s', date ('Y-m-d g:i a', $essay['revision']['time'])), $page, '<th class="fe8d" id="f10e" colspan="2">', '</th>');
   $page = inject ($_ee8f, $page, '<tbody id="dde6">', '</tbody>');

   return $page;
}

function render_explain ($page, $session) {

   if (! is_null ($session['person'])) {

      $page = str_replace ('<a class="e9f4 e5f7" href="/log-out">', '<a class="e9f4" href="/log-out">', $page);
      $page = str_replace ('<a class="e9f4" href="/log-in">', '<a class="e9f4 e5f7" href="/log-in">', $page);
      $page = inject ($session['person']['alias'], $page, '<span id="dda0">', '</span>');
   }

   return $page;
}

function render_greet ($page, $session, $resources) {

   $_9723 = '';

   foreach ($resources as $row) {

      $_e4da = sprintf ('<div class="ea1c">%s</div>', $row['name']);
      $_afc2 = sprintf ('<div class="c7f9">%s</div>', date ('j F Y', $row['time']));
      $_e9fa = sprintf ('<div class="b146"><a class="ed77" href="/retrieve/%s/%s">&#26597;&#30475;</a></div>', $row['digest'], $row['name']);
      $_61d4 = sprintf ('<img class="ef0e" src="/retrieve/7e7968ef712e7ff67730bd0481e66591e9acb62eddd43cdf77d74b2b78f3a7cc/clock.png">');
      $_d013 = sprintf ('<div class="e0c1">%s%s%s</div>', $_afc2, $_e9fa, $_61d4);
      $_19e8 = sprintf ('<div class="c375">%s%s</div>', $_e4da, $_d013);
      $_9723 = sprintf ('%s%s', $_9723, $_19e8);
   }

   $page = inject ($_9723, $page, '<div id="e5ab">', '</div><!-- -->');

   if (! is_null ($session['person'])) {

      $page = str_replace ('<a class="e9f4 e5f7" href="/log-out">', '<a class="e9f4" href="/log-out">', $page);
      $page = str_replace ('<a class="e9f4" href="/log-in">', '<a class="e9f4 e5f7" href="/log-in">', $page);
      $page = inject ($session['person']['alias'], $page, '<span id="dda0">', '</span>');
   }

   return $page;
}

function render_list_essays ($page, $session, $essays) {

   if (! is_null ($session['person'])) {

      $page = str_replace ('<a class="e9f4 e5f7" href="/log-out">', '<a class="e9f4" href="/log-out">', $page);
      $page = str_replace ('<a class="e9f4" href="/log-in">', '<a class="e9f4 e5f7" href="/log-in">', $page);
      $page = inject ($session['person']['alias'], $page, '<span id="dda0">', '</span>');
   }

   $_8b7d = '';
   
   foreach ($essays as $current) {
   
      $_ec05 = sprintf ('<div class="a163"><a class="bae6" href="/read/%s">%s</a></div>', $current['alias'], $current['alias']);
      $_8b7d = sprintf ('%s%s', $_8b7d, $_ec05);
   }
   
   $page = inject ($_8b7d, $page, '<div id="d28f">', '</div><!-- -->');

   return $page;
}

function render_log_in ($page, $session) {

   if (! is_null ($session['person'])) {

      $page = str_replace ('<a class="e9f4 e5f7" href="/log-out">', '<a class="e9f4" href="/log-out">', $page);
      $page = str_replace ('<a class="e9f4" href="/log-in">', '<a class="e9f4 e5f7" href="/log-in">', $page);
      $page = inject ($session['person']['alias'], $page, '<span id="dda0">', '</span>');
   }

   return $page;
}

function render_read ($page, $session, $essay) {

   if (! is_null ($session['person'])) {

      $page = str_replace ('<a class="e9f4 e5f7" href="/log-out">', '<a class="e9f4" href="/log-out">', $page);
      $page = str_replace ('<a class="e9f4" href="/log-in">', '<a class="e9f4 e5f7" href="/log-in">', $page);
      $page = inject ($session['person']['alias'], $page, '<span id="dda0">', '</span>');
   }

   $page = inject (mb_convert_encoding (parse_fefa ($essay['revision']['text']), 'HTML-ENTITIES', 'UTF-8'), $page, '<div id="ba3c">', '</div>');
   $page = str_replace ('<a class="f8d0" href="/" id="a68c">', sprintf ('<a class="%s" href="%s" id="a68c">', is_null ($session['person'])? 'dde5': 'f8d0', $essay['write']), $page);

   $page = inject ($essay['alias'], $page, '<strong id="b0d2">', '</strong>');
   $page = inject (is_null ($essay['revision'])? '': date ('Y-m-d g:i a', $essay['revision']['time']), $page, '<strong id="c833">', '</strong>');

   return $page;
}

function render_track ($page, $session) {

   return $page;
}

function render_warn ($page, $session) {

   if (! is_null ($session['person'])) {

      $page = str_replace ('<a class="e9f4 e5f7" href="/log-out">', '<a class="e9f4" href="/log-out">', $page);
      $page = str_replace ('<a class="e9f4" href="/log-in">', '<a class="e9f4 e5f7" href="/log-in">', $page);
      $page = inject ($session['person']['alias'], $page, '<span id="dda0">', '</span>');
   }

   return $page;
}

function render_write ($page, $session, $essay) {

   if (! is_null ($session['person'])) {

      $page = str_replace ('<a class="e9f4 e5f7" href="/log-out">', '<a class="e9f4" href="/log-out">', $page);
      $page = str_replace ('<a class="e9f4" href="/log-in">', '<a class="e9f4 e5f7" href="/log-in">', $page);
      $page = inject ($session['person']['alias'], $page, '<span id="dda0">', '</span>');
   }

   $page = inject (json_encode (array ('f275' => $essay['identity'])), $page, '<script type="application/json">', '</script>');
   $page = inject ($essay['alias'], $page, '<div id="e3de">', '</div>');
   $page = inject (mb_convert_encoding ($essay['revision']['text'], 'HTML-ENTITIES', 'UTF-8'), $page, '<textarea id="e916">', '</textarea>');

   return $page;
}
