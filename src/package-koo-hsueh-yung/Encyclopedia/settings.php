<?php

$_           = error_reporting (E_ALL);
$prefix_file = $_SERVER['C891'];
$prefix_web  = $_SERVER['E232'];
$remote      = $_SERVER['REMOTE_ADDR'];
$parameters  = $_REQUEST;

/* REMOTE_ADDR = 1D9D */
/* REQUEST_URI = 415C */
/* SERVER_NAME = 4594 */

require (sprintf ('%s/functions/curl.php', $prefix_file));
require (sprintf ('%s/functions/sqlite.php', $prefix_file));
require (sprintf ('%s/information.php', $prefix_file));
require (sprintf ('%s/modules/accomodation.php', $prefix_file));
require (sprintf ('%s/modules/publication.php', $prefix_file));
require (sprintf ('%s/miscellaneous.php', $prefix_file));
require (sprintf ('%s/prescription.php', $prefix_file));

$instance    = get_instance ($_SERVER['REQUEST_URI'], $_REQUEST, $_SERVER, $_SERVER['SERVER_NAME']);
$segments    = $instance[0];

$sqlite = new SQLite3 (sprintf ('%s/storage.sqlite', $prefix_file));
$_      = $sqlite->busyTimeout (6000);
$_      = date_default_timezone_set ('Asia/Singapore');


if (isset ($_COOKIE['b95a'])) {

   $session = lift (select_ec5f ($_COOKIE['b95a']));
}

if (! isset ($_COOKIE['b95a']) || is_null ($session) || $session['address'] != $_SERVER['REMOTE_ADDR']) {

   $identity = rand ();
   insert_8af1 ($_SERVER['REMOTE_ADDR'], $identity, 'zh', null, time ());
   setcookie ('b95a', $identity, 0, '/');
   $session = lift (select_ec5f ($identity));
}

if (! is_null ($session['person'])) {

   $session['person'] = lift (select_630e ($session['person']));
}

$result = execute_accomodation ($prefix_file, $prefix_web, $segments, $session);

if ($result[0] == 977895259) {

   $result = execute_publication ($prefix_file, $prefix_web, $segments, $session);
}

if ($result[0] == 977895259) {

   $result = array (62222576, 404, render_warn (file_get_contents (sprintf ('%s/pages/warn.html', $prefix_file)), $session));
}

if ($result[0] == 62222576) {

   http_response_code ($result[1]);
   print ($result[2]);
}

if ($result[0] == 681446758) {

   http_response_code ($result[1]);

   if (isset ($result[3]))
      insert_b2a1 (isset ($result[4])? $result[4]['identity']: $session['identity'], $result[3], time ());

   header (sprintf ('Location: %s', $result[2]));
   die ();
}

if ($result[0] == 1911304613) {

   http_response_code ($result[1]);
   print (json_encode ($result[2]));
   die ();
}
