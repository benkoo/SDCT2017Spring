<?php

function get_34a7 ($address) {

   $curl = curl_init ($address);
   $_    = curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
   $_    = curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
   $_    = curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);

   return curl_exec ($curl);
}

function get_ad9f ($address, $authentication) {

   $curl = curl_init ($address);
   $_    = curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
   $_    = curl_setopt ($curl, CURLOPT_USERPWD, $authentication);
   $_    = curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
   $_    = curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);

   return curl_exec ($curl);
}

function get_b7af ($address, $authentication, $headers) {

   $curl = curl_init ($address);
   $_    = curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
   $_    = curl_setopt ($curl, CURLOPT_USERPWD, $authentication);
   $_    = curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
   $_    = curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
   $_    = curl_setopt ($curl, CURLOPT_HTTPHEADER, $headers);

   return curl_exec ($curl);
}

function post_fbc2 ($address, $parameters, $headers = null, $authentication = null) {

   $string = '';

   foreach ($parameters as $key => $value) {

      $string = sprintf ('%s&%s=%s', $string, $key, urlencode ($value));
   }

   $string = substr ($string, 1);

   $curl = curl_init ();
   $_    = curl_setopt ($curl, CURLOPT_URL, $address);
   $_    = curl_setopt ($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
   $_    = curl_setopt ($curl, CURLOPT_POSTFIELDS, $string);
   $_    = curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
   $_    = curl_setopt ($curl, CURLOPT_SSLVERSION, 6);
   $_    = curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
   $_    = curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);

   if (! is_null ($authentication))
      { curl_setopt($curl, CURLOPT_USERPWD, $authentication); }

   return curl_exec ($curl);
}
