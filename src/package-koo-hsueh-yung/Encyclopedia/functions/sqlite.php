<?php

function execute_06c5 ($command, $parameters, $sqlite, $write) {

   foreach ($parameters as $key => $value)
      { $parameters[$key] = is_null ($parameters[$key])? 'NULL': sprintf ('%c%s%c', 39, strval ($sqlite->escapeString ($parameters[$key])), 39); }

   $command = call_user_func_array ('sprintf', array_merge (array ($command), $parameters));
   $result  = $sqlite->query ($command);

   return $write? $sqlite->changes (): extract_e03f ($result);
}

function extract_e03f ($result) {

   $rows = array ();

   while (1) {

      $item = $result->fetchArray (SQLITE3_ASSOC);

      if (is_bool ($item))
         { return $rows; }

      $rows[] = $item;
   }
}

function read_1f66 () {

   global $sqlite;

   $arguments = func_get_args ();

   return execute_06c5 ($arguments[0], array_slice (func_get_args (), 1), $sqlite, 0);
}

function write_1f21 () {

   global $sqlite;

   $arguments = func_get_args ();

   return execute_06c5 ($arguments[0], array_slice (func_get_args (), 1), $sqlite, 1);
}
