(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6781,        181]
NotebookOptionsPosition[      6373,        162]
NotebookOutlinePosition[      6729,        178]
CellTagsIndexPosition[      6686,        175]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{
  "code", "=", 
   "\"\<\n#ifdef USING_DOUBLE_PRECISIONQ\n#pragma OPENCL EXTENSION \
cl_khr_fp64 : enable\n#pragma OPENCL EXTENSION cl_amd_fp64 : enable\n#endif \
/* USING_DOUBLE_PRECISIONQ */\n__kernel void julia_kernel(__global Real_t * \
set, mint width, mint height, Real_t cx, Real_t cy) {\n   int xIndex = \
get_global_id(0);\n   int yIndex = get_global_id(1);\n   int ii;\n\n   Real_t \
x = ZOOM_LEVEL*(width/2 - xIndex);\n   Real_t y = ZOOM_LEVEL*(height/2 - \
yIndex);\n   Real_t tmp;\n   Real_t c;\n\n   if (xIndex < width && yIndex < \
height) {\n       for (ii = 0; ii < MAX_ITERATIONS && x*x + y*y < BAILOUT; \
ii++) {\n\t\t\ttmp = x*x - y*y + cx;\n\t\t\ty = 2*x*y + cy;\n\t\t\tx = tmp;\n\
\t\t}\n\t\tc = log(0.1f + sqrt(x*x + y*y));\n\t\tset[xIndex + yIndex*width] = \
c;\n    }\n}\n\>\""}], ";"}]], "Input"],

Cell[BoxData[{
 RowBox[{"Needs", "[", "\"\<OpenCLLink`\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"{", 
    RowBox[{"width", ",", "height"}], "}"}], "=", 
   RowBox[{"{", 
    RowBox[{"512", ",", "512"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"jset", "=", 
   RowBox[{"OpenCLMemoryAllocate", "[", 
    RowBox[{"Real", ",", 
     RowBox[{"{", 
      RowBox[{"height", ",", "width"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"JuliaCalculate", "=", 
   RowBox[{"OpenCLFunctionLoad", "[", 
    RowBox[{"code", ",", "\"\<julia_kernel\>\"", ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"_Real", ",", "_", ",", "\"\<Output\>\""}], "}"}], ",", 
       "_Integer", ",", "_Integer", ",", " ", "_Real", ",", " ", "_Real"}], 
      "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"16", ",", "16"}], "}"}], ",", 
     RowBox[{"\"\<Defines\>\"", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"\"\<MAX_ITERATIONS\>\"", "\[Rule]", "10"}], ",", 
        RowBox[{"\"\<ZOOM_LEVEL\>\"", "\[Rule]", "\"\<0.0050\>\""}], ",", 
        RowBox[{"\"\<BAILOUT\>\"", "\[Rule]", "\"\<4.0\>\""}]}], "}"}]}]}], 
    "]"}]}], ";"}]}], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Manipulate", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"JuliaCalculate", "[", 
     RowBox[{"jset", ",", " ", "width", ",", " ", "height", ",", " ", 
      RowBox[{"c", "[", 
       RowBox[{"[", "1", "]"}], "]"}], ",", " ", 
      RowBox[{"c", "[", 
       RowBox[{"[", "2", "]"}], "]"}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"width", ",", " ", "height"}], "}"}]}], "]"}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"ReliefPlot", "[", 
     RowBox[{
      RowBox[{"OpenCLMemoryGet", "[", "jset", "]"}], ",", " ", 
      RowBox[{"DataRange", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"-", "2.0"}], ",", " ", "2.0"}], "}"}], ",", " ", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"-", "2.0"}], ",", " ", "2.0"}], "}"}]}], "}"}]}], ",", 
      " ", 
      RowBox[{"ImageSize", "\[Rule]", "900"}], ",", 
      RowBox[{"ColorFunction", "\[Rule]", "\"\<SunsetColors\>\""}]}], "]"}]}],
    ",", "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"c", ",", " ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "1"}], "}"}]}], "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "2"}], ",", " ", 
       RowBox[{"-", "2"}]}], "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"2", ",", " ", "2"}], "}"}], ",", " ", "Locator"}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.68300478035007*^9, 3.6830047830834*^9}, {
  3.683004970840858*^9, 3.683004972847404*^9}}],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`c$$ = {0.5169999999999999, 
    1.2040000000000002`}, Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`c$$], {0, 1}}, {-2, -2}, {2, 2}}}, Typeset`size$$ = {
    900., {447., 452.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`c$15313$$ = {0, 0}}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`c$$ = {0, 1}}, 
      "ControllerVariables" :> {
        Hold[$CellContext`c$$, $CellContext`c$15313$$, {0, 0}]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, 
      "Body" :> ($CellContext`JuliaCalculate[$CellContext`jset, \
$CellContext`width, $CellContext`height, 
         Part[$CellContext`c$$, 1], 
         Part[$CellContext`c$$, 
          2], {$CellContext`width, $CellContext`height}]; ReliefPlot[
         OpenCLLink`OpenCLMemoryGet[$CellContext`jset], 
         DataRange -> {{-2., 2.}, {-2., 2.}}, ImageSize -> 900, ColorFunction -> 
         "SunsetColors"]), 
      "Specifications" :> {{{$CellContext`c$$, {0, 1}}, {-2, -2}, {2, 2}, 
         ControlType -> Locator}}, "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{951., {480., 485.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.683004776229343*^9, 3.683004783745612*^9}, 
   3.683004973757016*^9}]
}, Open  ]]
},
WindowSize->{Full, Full},
WindowMargins->{{148, Automatic}, {Automatic, 0}},
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 857, 14, 579, "Input"],
Cell[1418, 36, 1239, 33, 138, "Input"],
Cell[CellGroupData[{
Cell[2682, 73, 1565, 43, 117, "Input"],
Cell[4250, 118, 2107, 41, 982, "Output"]
}, Open  ]]
}
]
*)

