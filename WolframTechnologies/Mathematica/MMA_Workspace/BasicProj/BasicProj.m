

(* Mathematica Raw Program *)

A[0,n_] := n+1
A[m_,0] := A[m - 1, 1]
A[m_, n_] := A[m - 1, A[m, n - 1]]