(* ::Package:: *)

BeginPackage["Test`"]

PowerSum::usage = "PowerSum[x,n] returns the sum of the first n powers of x."

OtherSum::usage = "OtherSum[x,n] returns the sum of the first n powers of x"

OneMoreSum::usage = "OtherSum[x,n] returns the sum of the first n powers of x"

Begin["`Private`"]

(* A function for Power Sum *)
PowerSum[x_, n_] :=
   Module[{i},
      Sum[x^i, {i,1,n}]
]

(* Other function for Power Sum *)
OtherSum[x_, n_] :=
   Module[{i},
      Sum[x^2 i, {i,1,n}]
]

(* Other function for Power Sum *)
OneMoreSum[x_, n_] :=
   Module[{i},
      Sum[x^3 i, {i,1,n}]
]


End[]

EndPackage[]



